function checkData(data)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

originData = squeeze(data);
% combCoilData = squeeze(sum(abs(originData).*originData));
combCoilData = originData(1,:,:,:);
    
    onlyMNFdata = squeeze(combCoilData);
    
    stackRe = squeeze(sum(sum(abs(onlyMNFdata),2),3));
    [~, centerK] = max(stackRe);
    kcenData = squeeze(onlyMNFdata(centerK,:,:));
    
%     kcenData = squeeze(sum(onlyMNFdata,1));

figure;
subplot(2,1,1);plot(angle(kcenData(1,:)),'o');ylim([-pi pi]);grid on; title('shot1 Phase')
subplot(2,1,2);plot(angle(kcenData(2,:)),'o');ylim([-pi pi]);grid on;title('shot2 Phase')
%subplot(3,1,3);plot(angle(kcenData(3,:)),'o');ylim([-pi pi]);grid on;title('shot3 Phase')



end

