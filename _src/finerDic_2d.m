function [driftSpeed] = finerDic_2d(naCalData)
nTR = size(naCalData,3);
% nSl = size(naCalData,1);
if size(naCalData,1) == 2
combData = squeeze(sum(abs(naCalData).*naCalData));
else
combData = squeeze(naCalData(1,:,:));
end

sigPhase = angle(squeeze(combData(2,:)));
% allSlice = squeeze(sum(combData));
% sigPhase = angle(allSlice(2,:));
start = sigPhase(1);

%----------make dictionary ------------------------------------------------
nDrift = 200;

driftValue =linspace(-pi/500, pi/500, nDrift);
dictionary = zeros(nDrift, nTR);
lookuptable= zeros(nDrift, 1);

dictionaryIndex = 1;

for n = 1:nDrift
    for tr = 1:nTR
        dictionary(dictionaryIndex,tr) = angle(exp(1i*driftValue(n)*tr + 1i*start)); %+ 1i*startValue(m)));
    end
    lookuptable(dictionaryIndex,1) = driftValue(n);
    dictionaryIndex = dictionaryIndex +1;
end
% Normalize the dictionary and the signal.
for i = 1:size(dictionary,1)
    dictionary(i,:) = dictionary(i,:)/norm(dictionary(i,:));
end
sigPhase = sigPhase/norm(sigPhase);

%----------finish making it------------------------------------------------
% figure;plot(dictionary')
% hold on
% plot(sigPhase, "lineWidth",2)
% hold off
%----------match to diction------------------------------------------------
matchResult = dictionary*(sigPhase');
[~,pos] = max(matchResult);
% figure;plot(matchResult)

figure;plot(sigPhase,"o");
hold on;
plot(squeeze(dictionary(pos,:)),"lineWidth",2);
hold off;

driftSpeed = lookuptable(pos,1);
end

