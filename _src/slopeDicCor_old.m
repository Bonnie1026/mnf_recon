function [driftSpeed] = slopeDicCor(naCalData)
nTR = size(naCalData,3);
combData = squeeze(sum(abs(naCalData).*naCalData));
fidPhase = angle(squeeze(mean(combData)));
sigPhase = angle(squeeze(combData(2,:)));

meanPhase = sigPhase;
meanPhase(251:300) = fidPhase(251:300);
meanPhase(551:600) = fidPhase(551:600);
meanPhase(851:900) = fidPhase(851:900);
% meanPhase(1151:1200) = fidPhase(1151:1200);

%----------make dictionary ------------------------------------------------
nStart = 100;
nDrift = 100;

startValue =linspace(-pi,pi,nStart);
driftValue =linspace(-pi/40, pi/40, nDrift);
dictionary = zeros(nStart*nDrift, nTR);
lookuptable= zeros(nStart*nDrift,2);

dictionaryIndex = 1;
for m = 1:nStart
    for n = 1:nDrift
        for tr = 1:nTR
            dictionary(dictionaryIndex,tr) = angle(exp (1i*driftValue(n)*tr + 1i*startValue(m)));
        end
        lookuptable(dictionaryIndex,1) = driftValue(n);
        lookuptable(dictionaryIndex,2) = startValue(m);
        dictionaryIndex = dictionaryIndex +1;
    end
end

%----------finish making it------------------------------------------------

%----------match to diction------------------------------------------------
matchResult = dictionary*(meanPhase');
[~,pos] = max(matchResult);

figure;plot(meanPhase,'o');ylim([-pi pi]);grid on;
hold on;
plot(squeeze(dictionary(pos,:)),'LineWidth',4);
hold off;

driftSpeed = lookuptable(pos,1);
end

