function [pixalMap,shiftRange,mask] = getB0map(deltaTE,coilSelection,kspaceShift)
% Get B0 map from the B0 measurement
% B0Map : B0map, unit:Hz
% shiftRange : selected value for different B0 shift reconstructions.
% mask: mask out the backgroud area, also used for the MNF results.
%%-----------------------------------------------------------------------%%
% Steps for B0 shift values extraction. Efficient <-> Robust
%%-----------------------------------------------------------------------%%
stepNumber = 50;
% kspaceShift = -1.0;
%%-----------------------------------------------------------------------%%
% Create compression matrix:
%%-----------------------------------------------------------------------%%
Ub             = zeros(1000, 2);
Ub(1:500,1)    =  1;
Ub(501:1000,2) =  1;
%%-----------------------------------------------------------------------%%
% Load B0 measurement data:
%%-----------------------------------------------------------------------%%
disp('load B0 mapping data:')
B0Data = RawDataObjCentOut();
B0Data.selectCoils( coilSelection );
B0Data.filtering(true);
B0Data.removeOverSamplingFactor( 1.0 );
B0Data.applyPhaseCorrection(false);
B0Data.setKSpaceShift( kspaceShift );
B0Data.applyCompression(Ub);

nReB0 = B0Data.Dim.nRe;
nSlB0 = B0Data.Dim.nSl;
nChB0 = B0Data.Dim.nCh;
nTEB0 = B0Data.Dim.nSe;

trjB = get_trajectory(B0Data);
%%-----------------------------------------------------------------------%%
% Reconstruct the raw data, two results, one shorter TE, one longer TE
%%-----------------------------------------------------------------------%%
rawImgB = zeros(nSlB0,nTEB0,nChB0,2*nReB0,2*nReB0);
FTB0 = NUFFT(squeeze(trjB.k(1,:,:,1)), squeeze(trjB.w(1,:,:,1)), 1, 0, [2*nReB0,2*nReB0], 2);
for te=1:nTEB0
    for sl=1:nSlB0
        for ch=1:nChB0
            rawImgB(sl,te,ch,:,:) = FTB0'*double(squeeze(B0Data.data(1,sl,ch,:,:,te)));
        end 
    end
end
teShort = squeeze(rawImgB(1,1,:,:,:)); 
teLong  = squeeze(rawImgB(1,2,:,:,:));
%%-----------------------------------------------------------------------%%
% makeing the mask to set the background all zeros
%%-----------------------------------------------------------------------%%
mask = squeeze(sum(squeeze(abs(sum(rawImgB,2)))));
limB = mean(abs(mask(:)));
figure;imshow(mask,[1e-5 0.004]);title('check mask')
mask(mask > limB) = 1;
mask(mask < limB) = 0;
%%-----------------------------------------------------------------------%%
% get B0 map
%%-----------------------------------------------------------------------%%
imgPhaseDiff = zeros(nChB0,2*nReB0,2*nReB0);
for ch = 1:nChB0
    imgPhaseDiff(ch,:,:) = exp(1i*angle(teLong(ch,:,:))) .* exp(-1i*angle(teShort(ch,:,:)));
end
B0Map = angle(squeeze(sum(imgPhaseDiff)))/(2*pi*deltaTE);
B0Map = B0Map.*mask;

figure;imshow(B0Map,[-200 200]);title('B0 map')

%%-----------------------------------------------------------------------%%
% extract the shift values we plan to use for further reconstruction
%%-----------------------------------------------------------------------%%
minP = min(B0Map(:));
maxP = max(B0Map(:));

testRange = linspace(minP,maxP,stepNumber);
pixalMap = zeros(2*nReB0,2*nReB0);
for i = 1:2*nReB0
    for j = 1:2*nReB0
        [~,pos]       = min(abs(B0Map(i,j)+testRange));
        pixalMap(i,j) = testRange(pos);
    end
end
pixalMap = pixalMap.*mask;
shiftRange = unique(sort(nonzeros(pixalMap(:))));

testAdd = pixalMap + B0Map;
figure;imshow(testAdd, [-10 10]);title('check Diff map for B0')

end

