function [finalImg, mask] = get_SVD_images(MrData)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

%%-----------------------------------------------------------------------%%
% Extract dimensions
%%-----------------------------------------------------------------------%%
nRe = MrData.Dim.nRe;
nSl = MrData.Dim.nSl;
nCh = MrData.Dim.nCh;
nSe = MrData.Dim.nSe;
%%-----------------------------------------------------------------------%%
% Extract trajectory
%%-----------------------------------------------------------------------%%
trj = get_trajectory(MrData);
FT = NUFFT(squeeze(trj.k(1,:,:,1)), squeeze(trj.w(1,:,:,1)), 1, 0, [2*nRe,2*nRe], 2);

finalImg = zeros(nSl,2*nRe,2*nRe,nSe);
    for sl = 1:nSl
    %%-----------------------------------------------------------------------%%
    % Recon individual images
    %%-----------------------------------------------------------------------%%
        rawImg = zeros(nSe,nCh,2*nRe,2*nRe);
        for se=1:nSe
            for ch=1:nCh
                rawImg(se,ch,:,:) = FT'*double(squeeze(MrData.data(1,sl,ch,:,:,se)));
            end 
        end
    %%-----------------------------------------------------------------------%%
    % Estimate RX
    %%-----------------------------------------------------------------------%%
        sosFirstSVD = sqrt(abs(sum(rawImg(1,:,:,:).*conj(rawImg(1,:,:,:)),2)));
        resizeFirstSVD = repmat(sosFirstSVD,1,size(rawImg,2),1,1);
        imgRX = squeeze(rawImg(1,:,:,:)./resizeFirstSVD);
    %%-----------------------------------------------------------------------%%
    % match filter
    %%-----------------------------------------------------------------------%%
        sRx = squeeze(sum(imgRX.*conj(imgRX)));
        tmp = zeros(nCh,2*nRe,2*nRe);
        img = zeros(2*nRe,2*nRe,nSe);
        h = waitbar(0,'Match filter recon...ing');
        for se=1:nSe
            tmp = 0*tmp; %reset
            for ch=1:nCh
                tmp(ch,:,:) = double(squeeze(rawImg(se,ch,:,:)));
            end
            img(:,:,se) = squeeze(sum(tmp.*squeeze(conj(imgRX)),1))./squeeze(sRx);
            waitbar(se/nSe);
        end 
        close(h)
        finalImg(sl,:,:,:) = img;
    end
mask = squeeze(squeeze(abs(sum(img,3))));
lim = mean(abs(mask(:)))*2;
mask(mask > lim) = 1;
mask(mask < lim) = 0;

mask = repmat(mask,1,1,nSe,nSl);
mask = permute(mask,[4 1 2 3]);

end


