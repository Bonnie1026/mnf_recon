function plotMapSave(maps,saveMa)
% plot the MRF maps
figure(1); imshow(squeeze(maps(1,1,:,:)),[0, 1])  ; title('PD');colorbar
figure(2); imshow(squeeze(maps(2,1,:,:)),[0, 200]); title('T2');colorbar;colormap jet
figure(3); imshow(squeeze(maps(3,1,:,:)),[0,2000]); title('T1');colorbar;colormap jet
figure(4); imshow(squeeze(maps(5,1,:,:)),[0,90]); title('B1');colorbar
% 144:399,129:384
% 216:599,194:577
if saveMa
% Save the figures for PPT
    set(figure(1),'PaperSize', [3.2, 3.2]); saveas(figure(1),'2018_1202_IRFF_PD.png');
    set(figure(2),'PaperSize', [3.2, 3.2]); saveas(figure(2),'2018_1202_IRFF_T2.png');
    set(figure(3),'PaperSize', [3.2, 3.2]); saveas(figure(3),'2018_1202_IRFF_T1.png');
    set(figure(4),'PaperSize', [3.2, 3.2]); saveas(figure(4),'2018_1202_IRFF_B1.png');
end

end