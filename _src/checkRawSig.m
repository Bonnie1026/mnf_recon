function checkRawSig(rawdata)

coilData = squeeze((sum(sum(rawdata,5),6)))';
maxlim = max(abs(coilData(:)));
figure;plot(abs(coilData),'linewidth',1);ylim([0 maxlim]);grid on

end

