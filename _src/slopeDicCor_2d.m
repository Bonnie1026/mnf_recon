function [driftSpeed] = slopeDicCor_2d(naCalData)
nTR = size(naCalData,3);
% nSl = size(naCalData,1);
if size(naCalData,1) == 2
combData = squeeze(sum(abs(naCalData).*naCalData));
else
combData = squeeze(naCalData(1,:,:));
end
sigPhase = angle(squeeze(combData(2,:)));
% allSlice = squeeze(sum(combData));
% sigPhase = angle(allSlice(2,:));


%----------make dictionary ------------------------------------------------
nStart = 100;
nDrift = 100;

startValue =linspace(-pi,pi,nStart);
driftValue =linspace(-pi/40, pi/40, nDrift);
dictionary = zeros(nStart*nDrift, nTR);
lookuptable= zeros(nStart*nDrift,2);

dictionaryIndex = 1;
for m = 1:nStart
    for n = 1:nDrift
        for tr = 1:nTR
            dictionary(dictionaryIndex,tr) = angle(exp (1i*driftValue(n)*tr + 1i*startValue(m)));
        end
        lookuptable(dictionaryIndex,1) = driftValue(n);
        lookuptable(dictionaryIndex,2) = startValue(m);
        dictionaryIndex = dictionaryIndex +1;
    end
end

%----------finish making it------------------------------------------------
% figure;plot(dictionary')
%----------match to diction------------------------------------------------
matchResult = dictionary*(sigPhase');
[~,pos] = max(matchResult);

% figure;plot(matchResult)

figure;plot(sigPhase,"o");
hold on;
plot(squeeze(dictionary(pos,:)));
hold off;

driftSpeed = lookuptable(pos,1);
end

