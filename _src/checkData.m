function checkData(data)
% Check the Na data's phase
originData = squeeze(data);

if size(originData,1) == 2
combCoilData = squeeze(sum(abs(originData).*originData));
else
combCoilData = squeeze(originData(1,:,:,:));
end
    
kcenData = squeeze(combCoilData(2,:,:)); %Assume k = 2 always the k-space center

%     onlyMNFdata = squeeze(combCoilData);
%     stackRe = squeeze(sum(sum(abs(onlyMNFdata),2),3));
%     [~, centerK] = max(stackRe);
%     kcenData = squeeze(onlyMNFdata(centerK,:,:));
%     
%     kcenData = squeeze(sum(onlyMNFdata,1));

figure;
subplot(2,1,1);plot(angle(kcenData(1,:)),'o');ylim([-pi pi]);grid on; title('shot1 Phase')
subplot(2,1,2);plot(angle(kcenData(2,:)),'o');ylim([-pi pi]);grid on;title('shot2 Phase')
%subplot(3,1,3);plot(angle(kcenData(3,:)),'o');ylim([-pi pi]);grid on;title('shot3 Phase')



end

