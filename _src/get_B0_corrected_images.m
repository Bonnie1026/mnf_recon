function [finalImg] = get_B0_corrected_images(MrData,DWellTime, pixalMap, nFreqSteps)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

%%-----------------------------------------------------------------------%%
% Extract dimensions
%%-----------------------------------------------------------------------%%
nRe = MrData.Dim.nRe;
nSl = MrData.Dim.nSl;
nCh = MrData.Dim.nCh;
nSe = MrData.Dim.nSe;
nLi = MrData.Dim.nLi;
%%-----------------------------------------------------------------------%%
% Extract trajectory
%%-----------------------------------------------------------------------%%
trj = get_trajectory(MrData);
FT = NUFFT(squeeze(trj.k(1,:,:,1)), squeeze(trj.w(1,:,:,1)), 1, 0, [2*nRe,2*nRe], 2);

shots = nLi/1000;

stepN = length(nFreqSteps);
correctionShift = nFreqSteps*DWellTime*2*pi;%Rad

allImg   = zeros(stepN,nSl,2*nRe,2*nRe,nSe);
finalImg = zeros(nSl,2*nRe,2*nRe,nSe);
h = waitbar(0,'get different shifted images...');
for sl = 1:nSl
    for n = 1:stepN
        pShift = correctionShift(n);
        MrDataTemp.data = MrData.data;
    for sp = 1:shots
        for re = 1:nRe
            MrDataTemp.data(:,sl,:,re,(1000*(sp-1)+1):1000*sp,:) = MrDataTemp.data(:,sl,:,re,(1000*(sp-1)+1):1000*sp,:)*exp(1i*pShift*re*sp);
        end
    end
    %%-----------------------------------------------------------------------%%
    % Recon individual images
    %%-----------------------------------------------------------------------%%
        rawImg = zeros(nSe,nCh,2*nRe,2*nRe);
        for se=1:nSe
            for ch=1:nCh
                rawImg(se,ch,:,:) = FT'*double(squeeze(MrDataTemp.data(1,sl,ch,:,:,se)));
            end 
        end
    %%-----------------------------------------------------------------------%%
    % Estimate RX
    %%-----------------------------------------------------------------------%%
        sosFirstSVD = sqrt(abs(sum(rawImg(1,:,:,:).*conj(rawImg(1,:,:,:)),2)));
        resizeFirstSVD = repmat(sosFirstSVD,1,size(rawImg,2),1,1);
        imgRX = squeeze(rawImg(1,:,:,:)./resizeFirstSVD);
    %%-----------------------------------------------------------------------%%
    % match filter
    %%-----------------------------------------------------------------------%%
        sRx = squeeze(sum(imgRX.*conj(imgRX)));
        tmp = zeros(nCh,2*nRe,2*nRe);
        img = zeros(2*nRe,2*nRe,nSe);
        for se=1:nSe
            tmp = 0*tmp; %reset
            for ch=1:nCh
                tmp(ch,:,:) = double(squeeze(rawImg(se,ch,:,:)));
            end
            img(:,:,se) = squeeze(sum(tmp.*squeeze(conj(imgRX)),1))./squeeze(sRx);
    %         %check phase option:
    %         img(:,:,se) = sqrt(abs(sum(tmp.*conj(tmp),1))).*exp(1i*angle(sum(tmp,1)));
        end 
        allImg(n,:,:,:,:) = img;
    
%         upperlimit = max(abs(img(:)));
%         figure(5);imshow(squeeze(abs(img(:,:,1))),[0 upperlimit]);
        waitbar(n/stepN)
    end
close(h)
% save('shiftImgNew.mat','allImg');
%%-----------------------------------------------------------------------%%
% create final image
%%-----------------------------------------------------------------------%%
    for se = 1:nSe
        for i = 1:2*nRe
            for j = 1:2*nRe
                [~,pos] = min(abs(pixalMap(i,j)-nFreqSteps));
                finalImg(sl,i,j,se) = allImg(pos,sl,i,j,se);
            end
        end
    end
end

end

