%%-----------------------------------------------------------------------%%
%  Authors: Zidan Yu
%  Martijn.Cloos@nyumc.org
%  Date: 2018 Feb 28
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
clear all; 
close all; 
clc;
addpath(genpath('./_src'));

%%-----------------------------------------------------------------------%%
% Settings:
%%-----------------------------------------------------------------------%%
coilSelection               =  1:8;
kspaceShift                 = -1.0;
deltaTE                     = 0.0002; %s
DWellTime                   = 31200e-9; %s

%%-----------------------------------------------------------------------%%
% Get B0 map:
%%-----------------------------------------------------------------------%%
[B0Map,nFreqSteps,mask] = getB0map(deltaTE,coilSelection,kspaceShift);
% figure;imshow(B0Map,[-200 200]);title('B0 map')
%%-----------------------------------------------------------------------%%
% Load dictionary and compression matrix U:
%%-----------------------------------------------------------------------%%
load('./TR15compressedDictionaryandU/dictionaryTR15.mat')
load('./TR15compressedDictionaryandU/compressedU.mat')

% dictionary = IRFF_DictionaryObj('./uncompressed/','irff_dictionary.float');
% [U,~,~] = svd(dictionary.atoms,'econ');
% U = U(:,1:8);
% dictionary.atoms = transpose(transpose(dictionary.atoms)*U);
% for i=1:size(dictionary.atoms,2)
%     dictionary.atoms(:,i)= dictionary.atoms(:,i)/norm(dictionary.atoms(:,i));
% end


%%-----------------------------------------------------------------------%%
% Load raw data (VB or VD)
%%-----------------------------------------------------------------------%%
disp('load MNF data:')
MrData = RawDataObjCentOut();
MrData.selectCoils( coilSelection );
MrData.removeOverSamplingFactor( 1.0 ); %none is 1;
MrData.setKSpaceShift( kspaceShift );
MrData.applyPhaseCorrection(false);
MrData.applyCompression(U);

%%-----------------------------------------------------------------------%%
% Recon SVD images
%%-----------------------------------------------------------------------%%
img = get_B0_corrected_images(MrData,DWellTime, B0Map, nFreqSteps);

%%-----------------------------------------------------------------------%%
% creat the mask for object data
%%-----------------------------------------------------------------------%%
maskAdj = repmat(mask,1,1,size(img,1),size(img,4));
maskAdj = permute(maskAdj,[3 1 2 4]);
img = img.*maskAdj; 
%%-----------------------------------------------------------------------%%
% Check the image before matching
%%-----------------------------------------------------------------------%%
pmax = max(abs(img(:)));
figure;imshow(abs(squeeze(img(1,:,:,1))),[0 pmax])
% save('2018_11_19_MID274_img_50steps.mat','img')
%%-----------------------------------------------------------------------%%
% matching
%%-----------------------------------------------------------------------%%
maps = zeros(5,size(img,1),size(img,2),size(img,3));
h = waitbar(0,'MRF matching');

for sl=1:size(img,1)
    for i=1:size(img,2)
        waitbar(i/size(img,3));
        for j=1:size(img,3)
            if mask(i,j) ~= 0
                [pd,pos] = max(squeeze(img(sl,i,j,:))'*dictionary.atoms);
                maps(:,sl,i,j) = dictionary.lookup(:,pos);
                maps(1,sl,i,j) = abs(pd)/maps(1,sl,i,j);
            else
                maps(:,sl,i,j) = 0;
            end
        end   
    end
end

close(h);
maps(1,:,:,:) = squeeze(maps(1,:,:,:)./(max(abs(squeeze(maps(1,:))))));
% save('20181119_MID274_B0cor200mius.mat' ,'maps','-v6');
%%-----------------------------------------------------------------------%%
% Plot Figure and save data
%%-----------------------------------------------------------------------%%
plotMapSave(maps,false);