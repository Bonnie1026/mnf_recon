%%-----------------------------------------------------------------------%%
%  Authors: Zidan Yu & Martijn Cloos
%  Zidan.Yu@nyulangone.org
%  Date: 2018 Feb 28
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
clear all; 
close all; 
clc;
addpath(genpath('./_src'));

%%-----------------------------------------------------------------------%%
% Settings:
%%-----------------------------------------------------------------------%%
coilSelection               =   9:16; % select the sodium coils
HammingFilter               =   true;
oversamplingReductionFactor =    1.0;
kspaceShift                 =   -0.7;
sliceForDisplay             =      1;
PhaseCorrection             =   true;

naFreq = 78.613000;
hFreq  = 297.194280;
ratioFreq = naFreq/hFreq;
%%-----------------------------------------------------------------------%%
% Create compression matrix:
%%-----------------------------------------------------------------------%%
U = ones(1150, 1);
%%-----------------------------------------------------------------------%%
% Load raw data (VB or VD)
%%-----------------------------------------------------------------------%%
MrData = RawDataObjCentOut();
MrData.selectCoils( coilSelection );
% MrData.coil_compression(2); % can use this if you have too many coil elements
checkData(MrData.data)
% %%-------Correct the FLASH segments, and Check---------------------------%%
MrData.FLASHcorrect(MrData.rfPhase);
% checkData(MrData.data)
%%-------Dictionary correcion for phase, and Check-----------------------%%
if (PhaseCorrection)
MrData.rewindPhase(slopeDicCor_2d(squeeze(MrData.data(:,:,:,:,2,:))));
MrData.rewindPhase(finerDic_2d(squeeze(MrData.data(:,:,:,:,2,:))));
end
checkData(MrData.data)
%%-------Align all shots, and Check--------------------------------------%%
MrData.alineShotPhase(true); %should always be the last step of correction
checkData(MrData.data);
checkRawSig(MrData.data);
drawnow
%%-------Process the data for recon--------------------------------------%%
MrData.filtering(HammingFilter);
MrData.removeOverSamplingFactor( oversamplingReductionFactor ); %none is 1;
MrData.setKSpaceShift( kspaceShift );
MrData.applyCompression(U);
%%-----------------------------------------------------------------------%%
% Extract dimensions
%%-----------------------------------------------------------------------%%
nRe = MrData.Dim.nRe;
nSl = MrData.Dim.nSl;
nCh = MrData.Dim.nCh;
nSe = MrData.Dim.nSe;
nRe_new = floor(ratioFreq*nRe);
%%-----------------------------------------------------------------------%%
% Extract trajectory
%%-----------------------------------------------------------------------%%
trj = get_trajectory(MrData);
%%-----------------------------------------------------------------------%%
% Recon individual images
%%-----------------------------------------------------------------------%%
FT = NUFFT(squeeze(trj.k(1,:,:,1)), squeeze(trj.w(1,:,:,1)), 1, 0, [nRe_new,nRe_new], 2);
img = zeros(nSl,nRe_new,nRe_new); 
for sl=1:nSl
    rawImg = zeros(nCh,nRe_new,nRe_new);
    for ch=1:nCh
        rawImg(ch,:,:) = FT'*double(squeeze(MrData.data(1,sl,ch,:,:,1)));
    end 
    %%-----------------------------------------------------------------------%%
    % Estimate RX
    %%-----------------------------------------------------------------------%%
    sosImg = sqrt(abs(sum(rawImg.*conj(rawImg))));
    sosImg = repmat(sosImg,size(rawImg,1),1,1);
    imgRX = squeeze(rawImg./sosImg);
    
    %%-----------------------------------------------------------------------%%
    % match filter
    %%-----------------------------------------------------------------------%%
    sRx = squeeze(sum(imgRX.*conj(imgRX)));
    tmp = zeros(nCh,nRe_new,nRe_new);
        for ch=1:nCh
            tmp(ch,:,:) = double(squeeze(rawImg(ch,:,:)));
        end
        img(sl,:,:) = squeeze(sum(tmp.*squeeze(conj(imgRX)),1))./squeeze(sRx);
%         img(:,:,se) = sqrt(abs(sum(tmp.*conj(tmp),1))).*exp(1i*angle(sum(tmp,1)));

end 

%%-----------------------------------------------------------------------%%
% Plot and Save figure for Log .ppt
%%-----------------------------------------------------------------------%%
maxSignal = max(abs(img(:)));
figure; imshow(abs(squeeze(img)),[0 maxSignal]);

save('MID437_Na_te3p5.mat','img','trj');

% To save image directly to a .png
% img = 180 * squeeze(img)/max(img(:));
%255 = white
%    0 = black
% imwrite(uint8(img), 'myImage.png');