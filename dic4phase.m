clear all
close all
clc

nTR = 1200;
start =linspace(-pi,pi,50);
drift =linspace(-pi/40, pi/40, 100);
dictionary = zeros(length(start),length(drift),nTR);

for m = 1:length(start)
    for n = 1:length(drift)
        onedic = zeros(1,nTR);
        for tr = 1:nTR
            onedic(tr) = exp (1i*drift(n)*tr + 1i*start(m) );
        end
        dictionary(m,n,:) = onedic;
    end
end
lookup = drift;
        
