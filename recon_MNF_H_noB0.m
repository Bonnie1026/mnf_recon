%%-----------------------------------------------------------------------%%
%  Authors: Zidan Yu & Martijn Cloos
%  Zidan.Yu@nyulangone.org
%  Date: 2018 Feb 28
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
clear all; 
close all; 
clc;
addpath(genpath('./_src'));

%%-----------------------------------------------------------------------%%
% Settings:
%%-----------------------------------------------------------------------%%
coilSelection               =   1:8; % select the proton coils
kspaceShift                 = -1.15;
remove_os_after__FT         =  true;
%%-----------------------------------------------------------------------%%
% Load dictionary and compression matrix U:
%%-----------------------------------------------------------------------%%
dictionary = SVD_DictionaryObj('./TR7p5_2019_2%_TB3/','IRFF_TR7p5_2p5_BW3.dictionary');
dictionary.atoms = dictionary.atoms;
svd_range = size(dictionary.atoms,1);
U = dictionary.U;
%%-----------------------------------------------------------------------%%
% Load raw data (VB or VD)
%%-----------------------------------------------------------------------%%
disp('load MNF data:')
MrData = RawDataObjCentOut();

% Pick data for the proton side------------------------------------------%%
MrData.data = MrData.data(:,:,:,:,:,[1:250 301:550 601:850 901:1150]);
MrData.angles = MrData.angles(:,:,[1:250 301:550 601:850 901:1150]);
MrData.Dim.nSe = 1000;
%%-----------------------------------------------------------------------%%
MrData.selectCoils( coilSelection );
MrData.removeOverSamplingFactor( 1.0 ); % none is 1;
MrData.setKSpaceShift( kspaceShift );
MrData.applyPhaseCorrection(false);
MrData.applyCompression(U);

%%-----------------------------------------------------------------------%%
% Recon SVD images
%%-----------------------------------------------------------------------%%
[img,mask] = get_SVD_images(MrData);
%%-----------------------------------------------------------------------%%
% Check the image before matching
%%-----------------------------------------------------------------------%%
pmax = max(abs(img(:)));
figure;imshow(abs(squeeze(img(1,:,:,1))),[0 pmax])
%     save("IMG_H_MNF_1213_corA15_oldcoil_MID198.mat","img","mask","-v6");
%%-----------------------------------------------------------------------%%
% Release memory
%%-----------------------------------------------------------------------%%
clear('FT');
clear('tmp');
clear('MrData');
%%-----------------------------------------------------------------------%%
% Remove oversampling
%%-----------------------------------------------------------------------%%
if remove_os_after__FT
    parI = round(size(img,2)/4);
    staI = parI+1;
    endI = round(3*parI);
    img  = img(:,staI:endI,staI:endI,:);
    mask = mask(:,staI:endI,staI:endI,:);
end

%%-----------------------------------------------------------------------%%
% matching
%%-----------------------------------------------------------------------%%
img = img.*mask;
img = single(img); %makes the matching much faster 
maps = zeros(5,size(img,1),size(img,2),size(img,3));

for i=1:size(img,1)
    [map, ~] = fast_match(real(img(i,:,:,:)), dictionary,1000);
    maps(:,i,:,:) = map;
    disp(num2str(i));
end
%%-----------------------------------------------------------------------%%
% Plot Figure and save data
%%-----------------------------------------------------------------------%%
plotMapSave(maps,false); % can adjust the scales for the maps in this function @ ./_src/plotMapSave.m
save('MID437_H_te3p5.mat','maps');